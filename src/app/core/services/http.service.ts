import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { debounceTime, map } from 'rxjs/operators';


const API_URL: string = 'https://api3.aumet.me/api';
const API_URL2: string = 'https://api2.aumet.me/api';

@Injectable()
export class HttpService {

  constructor(private http: HttpClient) {}
  

  search(key: string) {
    let search_url = `${API_URL}/ScientificNames/SearchElsatic?ProspectedCompanyId=214388`
    return this.http.get(`${search_url}&key=${key}`).pipe(
      debounceTime(500),
      map((res: any) => res && res.data || []),
    );
  }


  getCompanyGallery() {
    let api = `${API_URL2}/Profile/GetCompanyPhotos?companyslug=human-biosciences-inc&compId=`;
    return this.http.get(api).pipe();
  }

  getProductsList() {
    let api = `${API_URL2}/Profile/GetManufacturerProducts?companyslug=human-biosciences-inc&compId=`;
    return this.http.get(api);
  }

}