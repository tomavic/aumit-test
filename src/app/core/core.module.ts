import { NgModule, Optional, SkipSelf, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { HttpService } from './services/http.service';
import { throwIfAlreadyLoaded } from './module-import-guard';


const APP_PROVIDERS = [
  HttpService
];


@NgModule({
  imports: [
    CommonModule, 
    HttpClientModule, 
    RouterModule,
  ],
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [...APP_PROVIDERS]
    } as ModuleWithProviders;
  }
}
