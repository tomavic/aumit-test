
/**
 * @class ngx-bootstrap 
 * @description shared module contains all used modules via ngx-bootstrap library
 * @note We use this way of import because Angular 9 doesn't support this kind of import
 */

import { TypeaheadModule } from 'ngx-bootstrap/typeahead';


export const NGX_BOOTSRAP_MODULES = [
  TypeaheadModule.forRoot(),
]