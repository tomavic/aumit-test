import { Component } from "@angular/core";
import { HttpService } from "./core/services/http.service";

import { Observable, Observer, of } from "rxjs";
import { map, switchMap, tap } from "rxjs/operators";
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead/typeahead-match.class';

class DataSourceType {
  id: number;
  name: string;
  distributorCount: number;
  imagePath: string;
  createdAt: string;
  createdBy: string;
  token: string;
}


@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  search: string;
  suggestions$: Observable<DataSourceType>;
  errorMessage: string;
  selected: DataSourceType;
  galleryList: Array<any>;
  productList: Array<any>;

  constructor(private httpService: HttpService) {}

  ngOnInit(): void {

    // get gallery and product lists
    this.getGallery();
    this.getProducts();

    // start Observable
    this.suggestions$ = new Observable((observer: Observer<string>) => {
      observer.next(this.search);
    }).pipe(
      switchMap((query: string) => {
        if (query) {
          return this.httpService.search(query).pipe(
            map((data: any) => data),
            tap((res) => {
              console.log(res)
            },
            err => {
              // in case of http error
              this.errorMessage = err && err.message || 'Something goes wrong';
            })
          );
        }
 
        return of([]);
      })
    );
  }

  /**
   * @function onSelect
   * @description fired when user selects option from suggestions$ list
   * @param {TypeaheadMatch} event 
   */
  onSelect(event: TypeaheadMatch): void {
    this.selected = event.item;
  }


  /**
   * @function getGallery
   * @description get the gallery list and append it to the view using galleryList
   */
  getGallery() {
    this.httpService.getCompanyGallery().subscribe((res: any) => {
      this.galleryList = res.documents.CompanyOffice;
    })
  }

  /**
   * @function getProducts
   * @description get the products list and append it to the view using productList
   */
  getProducts() {
    this.httpService.getProductsList().subscribe((res: any) => {
      this.productList = res[0].products;
    })
  }


}
